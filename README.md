# Node JS

## How Node JS works ?

* Single thread

* Non blocking I/O

## The Node core

### Global object

* __dirname: The directory name of the current module

* __filename: The file name of the current module

* exports, require

### Process object

* process.argv: returns an array containing the command line arguments passed when the Node.js process was launched

* process.stdout: print to command line

* process.stdin.on(data, function): get input from command line

* process.on(exit): exit

### Global timing

* setTimeout(function, waitTime): waiting in miliseconds

* setInterval(function, waitInterval): repeating in miliseconds

    + stop repeating: use clearInterval

## Node modules

* path: pluck the base file name from a full path

* util: several helper functions that we can use (log: add date and timestamp)

* v8: get memory usage statistics


### Readline

* provides an interface for reading data from a Readable stream one line at a time

### EventEmitter

```
emitter.on(event, function(message, status));

emitter.emit(event, message, status);

// inherits 

util.inherits(Person, EventEmitter); // Person now inherits from EventEmitter

var ben = new Person('name');
ben.on(event, function(message));
```

### Custom modules

* use require() and module.exports

### Creating child process

* exec:
```
exec()
```

* spawn:
```
spawn('node', [])
```

## File System Module

### Listing directory files

```
fs.readdirSync(path) (block nodejs thread)
fs.readdir(path, callback)
```

### Reading files
```
fs.readFileSync(file) (block nodejs thread)
fs.readFile(file, 'utf-8', callback)
```

### Writing and appending files
```
fs.writeFile(file, callback)
fs.appendFile(file, callback)
```

### Directory creation
```
fs.existsSyn(dir)
fs.mkdir(dir, callback)
```

### Renaming and removing files
```
fs.renameSync(old_file, new_file)
fs.renameSync(old_file, new_file, callback)

fs.unlinkSync(file)
fs.unlink(file, callback)
```

### Renaming and removing directories
```
fs.renameSync(dir, new_dir)
fs.rename(dir, new_dir, callback)

fs.rmdirSync(dir)
fs.rmdir(dir, callback)
```

### Readable file streams

* fs.createReadStream(file)  
     

### Writable file streams

* fs.createWriteStream(file)

## HTTP Module

### Making a request
```
req = https.request(options, function() {
    res.on('data', callback);
    res.on('end', callaback);
})
```

### Building a web server
```
var server = http.createServer(function(req, res) {

});

server.listen(3000);
```

### Serving files
```
// HTML files
fs.readFile("./public/index.html", "UTF-8", function(err, html) {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(html);
});

// CSS files
var fileStream = fs.createReadStream(path, "UTF-8");
res.writeHead(200, {"Content-Type": "text/css"});
fileStream.pipe(res);

// JSON
res.writeHead(200, {"Content-Type": "text/json"});
res.end(JSON.stringify(data));
```

## Web Servers

### Intro to Express

* middleware: app.use()

* server static files: app.use(express.static())

* cross domain: app.use(cors())

* get: app.get(url, callback)

## Web Sockets

* create persistent connection

* broadcast changes to all connections

* SocketIO fails back to long polling when websockets aren't supported